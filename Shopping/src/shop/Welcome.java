package shop;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * Servlet implementation class Welcome
 */
public class Welcome extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Welcome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String a=request.getParameter("uname");

		HttpSession s=request.getSession();
		s.setAttribute("uname", a);
		RequestDispatcher rd=request.getRequestDispatcher("Rules.html");
		rd.forward(request, response);
//		PrintWriter out=response.getWriter();	
//		out.println("<center><h3> Welcome to</h3><br><br><br><h1> Cheap Shop</h1><br><br><br>"+a+"</center>");
//		out.println("<br><br><center><a href='Rules.html'>Let's Go</a></center>");
		
		
	}

}
