package com.javatraining.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.javatraining.custmodel.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) 
    {
    	Customer c= new Customer(1, "Noel", "Vizag", 2401);
//		This will search for the hibernate.cfg.xml and load the dbconfiguartion
    	Configuration config=new Configuration().configure();
    	SessionFactory sf=config.buildSessionFactory();
    	Session s=sf.openSession();
    	Transaction tr=s.beginTransaction();
    	s.save(c);
    	tr.commit();
    	System.out.println("Data stored");
    	s.close();
    	sf.close();
    	
    }
}
