package jdbc;

import java.sql.*;
import java.util.*;

import com.javatraining.dbcon.Dbconfig;

public class Demo2 {
	public static void main(String[] args) throws SQLException {
		
		Demo2 d2= new Demo2();
		
		Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter Customer ID:");
		int customerId=sc.nextInt();
		
		System.out.println("Enter Customer Name:");
		String customerName=sc.next();
		
		System.out.println("Enter Customer Address:");
		String customerAddress=sc.next();
		
		System.out.println("Enter Bill Amount:");
		int billAmount=sc.nextInt();
		
		jdbc.Demo2.insertCustDetails(customerId, customerName, customerAddress, billAmount);
	}

	public static void insertCustDetails(int customerId, String customerName, String customerAddress, int billAmount )
			throws SQLException {
		Connection conn=Dbconfig.getConnection();
		PreparedStatement s=conn.prepareStatement("insert into customer values(?,?,?,?)");
		
		s.setInt(1, customerId);
		s.setString(2, customerName);
		s.setString(3, customerAddress);
		s.setInt(4, billAmount);
		int rows=s.executeUpdate();
		
		System.out.println(rows+ "Data Inserted");
	}
	
	public static void insertCustDetails(Customer c)
			throws SQLException {
		Connection conn=Dbconfig.getConnection();
		PreparedStatement s=conn.prepareStatement("insert into customer values(?,?,?,?)");
		
		s.setInt(1, c.getCustomerId());
		s.setString(2, c.getCustomerName());
		s.setString(3, c.getCustomerAddress());
		s.setInt(4, c.getBillAmnt());
		int rows=s.executeUpdate();
		
		System.out.println(rows+ "Data Inserted");
	}

}
