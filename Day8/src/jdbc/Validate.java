package jdbc;
import java.sql.*;
import java.util.*;

import com.javatraining.dbcon.Dbconfig;

public class Validate {

	public static void main(String[] args) throws SQLException {
		
		Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter the userName:");
		String user= sc.nextLine();
		
		System.out.println("Enter the password:");
		String pass=sc.next();
		
		String q1="select * from users where userName=? and password=?";
		
		Connection c= Dbconfig.getConnection();
		PreparedStatement s=c.prepareStatement(q1);
		s.setString(1, user);
		s.setString(2, pass);
		
		ResultSet res=s.executeQuery();
		
		if(res.next())
		{
			System.out.println("Valid userName and Password.");
		}
		else
		{
			System.out.println("InValid userName and Password.");
		}
			
		res.close();
		s.close();
		c.close();
		
		
		


	}

}
