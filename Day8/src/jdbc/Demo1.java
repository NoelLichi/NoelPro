package jdbc;

import java.sql.*;

import com.javatraining.dbcon.Dbconfig;

public class Demo1 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		{
			Connection conn= Dbconfig.getConnection();
		
			Statement s=conn.createStatement();
			ResultSet res=s.executeQuery("select * from customer");
			
			while(res.next())
			{
				System.out.print(res.getString(1));
				System.out.print(" \t"+res.getString(2));
				System.out.print("\t\t"+res.getString(3));
				System.out.println(" \t"+res.getString(4));
			}
			
			res.close();
			s.close();
			conn.close();
		
		
		
		
		
		}
		// TODO Auto-generated method stub
		

	}

}
