package jdbc;

import java.sql.SQLException;
import java.util.Scanner;

public class CustDB {

	public static void main(String[] args) throws SQLException {
Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter Customer ID:");
		int customerId=sc.nextInt();
		
		System.out.println("Enter Customer Name:");
		String customerName=sc.next();
		
		System.out.println("Enter Customer Address:");
		String customerAddress=sc.next();
		
		System.out.println("Enter Bill Amount:");
		int billAmount=sc.nextInt();

		Customer c= new Customer();
		c.setCustomerId(customerId);
		c.setCustomerName(customerName);
		c.setCustomerAddress(customerAddress);
		c.setBillAmnt(billAmount);
		
		Demo2.insertCustDetails(c);
		
		
		
		
	}

}
