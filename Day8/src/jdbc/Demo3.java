package jdbc;

import java.sql.*;
import java.util.*;

import com.javatraining.dbcon.Dbconfig;

public class Demo3 {
	
	public static void main(String[] args) throws SQLException {
		
		
	Scanner sc=new Scanner(System.in);
	
	System.out.println("Enter the Customer Id:");
	int customerId=sc.nextInt();
	

	
	
	System.out.println("Enter the address to update:");
	String customerAddress=sc.next();
	
	System.out.println("Enter the Bill Amount:");
	int billAmount=sc.nextInt();
		
		
		String q1="update customer set customerAddress=?, billAmount=? where customerId=?";
	
		Connection c= Dbconfig.getConnection();
		
		
	
		PreparedStatement s= c.prepareStatement(q1);
		
		
		s.setInt(3, customerId);
		s.setString(1, customerAddress);
		s.setInt(2, billAmount);
		
		int rows= s.executeUpdate();
		System.out.println(rows+ " rows updated");
		
		c.close();
		
		
		
		
	}

}
