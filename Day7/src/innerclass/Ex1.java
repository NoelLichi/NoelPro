package innerclass;

public class Ex1 {
	
	int j=100;
	
	private class Inner{
		
		int i=90;
		public void show() {
			
			System.out.println("The value of i is:"+i);
			System.out.println("The value of j is:"+j);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Ex1 i= new Ex1();
		Ex1.Inner a= i.new Inner();
		
		a.show();
		

	}

}
