package thread;

public class Thread2 extends Thread{
	
	public Thread2() {
		super("ITPL1"); //or super(""+i);	
		start();
		
		}
public Thread2(String x) {
		super(x);
		start();
		
		
	}
@Override
public void run() {
	System.out.println("Run called: "+Thread.currentThread().getName());
	
}
	public static void main(String[] args) {
		
		new Thread2("1");
		new Thread2("2");
		new Thread2("3"); /*or for(i=1, i<5;i++) new Thread2(i);*/
		new Thread2("4");
		new Thread2("5");
		
		System.out.println("Main called: "+Thread.currentThread().getName());	
	}

}
