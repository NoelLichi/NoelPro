package thread;

public class Thread3  extends Thread{
	
	
	
	public synchronized static  void display(String threadName, String message1,String message2)
	{
		System.out.println("Message by: "+threadName+" thread");
		System.out.println("Message 1,by thread "+threadName+" is "+message1);
		try {
			Thread.sleep(7000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Message 2, by thread "+threadName+" is " +message2);
	}
	
	public Thread3(int i) {
		super(""+i);	
		start();
		}
	
	@Override
	public void run() {
		display(Thread.currentThread().getName(),"Hi","Bye");
		
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for(int i=1;i<5;i++) 
			new Thread3(i);

	}

}
