package generics;

public class Exmpl1<Z> {
	
	public void show(Z  z) {
		
		System.out.println(z);
	}

	public static void main(String[] args) {
		Exmpl1<Boolean> ex1= new Exmpl1<Boolean>();
		ex1.show(true);
		Exmpl1<String> ex2= new Exmpl1<String>();
		ex2.show("Hey  this is noel");
		Exmpl1<Integer> ex3= new Exmpl1<Integer>();
		ex3.show(17);

	}

}
