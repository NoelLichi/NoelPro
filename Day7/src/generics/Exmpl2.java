package generics;

public class Exmpl2<A> {
	


	public <A extends Number> 
	
	  double dsip(A one, A two){
		return one.doubleValue()+two.doubleValue();
	}
	
	
	public static void main(String[] args) {
		
		
		Exmpl2<Integer> e1= new Exmpl2<Integer>();
		System.out.println(e1.dsip(10,177));
	
		
		
	}

}
