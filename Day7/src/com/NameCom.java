package com;

import java.util.Comparator;

public class NameCom implements Comparator<Customer> {

	@Override
	public int compare(Customer o1, Customer o2) {
		if(o1.getCustomerName().compareTo(o2.getCustomerName())>0)
		return 1;
		else return -1;
	}
	
	

}
