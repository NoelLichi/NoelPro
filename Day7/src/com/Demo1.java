package com;

import java.util.*;

public class Demo1 {

	public static void main(String[] args) {
		
		
		Customer c1=new Customer(1,"Mohan","Pune",9800);
		Customer c2=new Customer(2,"Anu","Mumbai",800);
		Customer c3=new Customer(3,"Zeba","Agra",1800);
		Customer c4=new Customer(4,"Uday","Jaipur",2900);
		Customer c5=new Customer(5,"Neeti","Delhi",200);

		List<Customer> allC=new ArrayList<Customer>();
		
		allC.add(c1);
		allC.add(c2);
		allC.add(c3);
		allC.add(c4);
		allC.add(c5);
		
		Collections.sort(allC, new AddressCom());
		System.out.println("After sorting basing on Address:");
		System.out.println(allC);
		
		Collections.sort(allC, new BillamountCom());
		System.out.println("After sorting basing on BillAmounts:");
		System.out.println(allC);
		
		Collections.sort(allC, new NameCom());
		System.out.println("After sorting basing on Names:");
		System.out.println(allC);
		
		
		
		Collections.sort(allC, new Comparator<Customer>()
		
		{

			@Override
			public int compare(Customer o1, Customer o2) {
				if(o1.getCustomerId()<o2.getCustomerId())
				return 0;
				else return -1;
			}
			
			
			
		});
		System.out.println("After sorting basing on Id's:");
		System.out.println(allC);
		
		
		
		
	
		/*Iterator i= allC.iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
			}*/
		
		//Noel Lichi's Code
		
	}

}
