package files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileEx2 {

	public static void main(String[] args) throws IOException{
		
		String readF= "data.txt";
		String writeF= "newdata.txt";
		
		File fe1= new File(readF);
		File fe2= new File(writeF);
		FileInputStream f1= new FileInputStream(fe1);
		FileOutputStream f2= new FileOutputStream(fe2);
		
		System.out.println("File is Copied");
		
		int c=0;
		
		while ((c=f1.read())!=-1)
		{
			f2.write((char)c);
			
		}
		f1.close();
		f2.close();
				
				


	}

}
