package files;
import java.io.*;

public class Ex3 {

	public static void main(String[] args) throws IOException {
		int sal=96000;
		boolean male= true;
		double average=98.8;
		
		DataOutputStream s=new DataOutputStream(new BufferedOutputStream(new FileOutputStream("take.txt")));
		
		s.writeInt(sal);
		s.writeBoolean(male);
		s.writeDouble(average);
		s.close();
		System.out.println("Done");
	}

}
