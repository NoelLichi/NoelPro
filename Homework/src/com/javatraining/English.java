package com.javatraining;

public class English {
private int Emarks;
private String Egrade;
public int getEmarks() {
	return Emarks;
}
public void setEmarks(int emarks) {
	Emarks = emarks;
}
public String getEgrade() {
	return Egrade;
}
public void setEgrade(String egrade) {
	Egrade = egrade;
}
@Override
public String toString() {
	return "English Marks = " + Emarks + ", Egrade=" + Egrade;
}
public English(int emarks, String egrade) {
	super();
	Emarks = emarks;
	Egrade = egrade;
}

}
