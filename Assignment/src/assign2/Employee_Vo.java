package assign2;

public class Employee_Vo {
		private int empid;
		private String empname;
		private int annualincome;
		private int incometax;
		
		public Employee_Vo() {
			
		}

		public Employee_Vo(int empId, String empName, int annualincome, int incometax) {
			super();
			this.empid = empId;
			this.empname = empName;
			this.annualincome = annualincome;
			this.incometax = incometax;
		}
		
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + annualincome;
			result = prime * result + empid;
			result = prime * result + ((empname == null) ? 0 : empname.hashCode());
			result = prime * result + incometax;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Employee_Vo other = (Employee_Vo) obj;
			if (annualincome != other.annualincome)
				return false;
			if (empid != other.empid)
				return false;
			if (empname == null) {
				if (other.empname != null)
					return false;
			} else if (!empname.equals(other.empname))
				return false;
			if (incometax != other.incometax)
				return false;
			return true;
		}

		public int getEmpid() {
			return empid;
		}
		
		
		public void setEmpid(int empid) {
			this.empid = empid;
		}
		public String getEmpname() {
			return empname;
		}
		public void setEmpname(String empname) {
			this.empname = empname;
		}
		public int getAnnualincome() {
			return annualincome;
		}
		public void setAnnualincome(int annualincome) {
			this.annualincome = annualincome;
		}
		public int getIncometax() {
			return incometax;
		}
		public void setIncometax(int incometax) {
			this.incometax = incometax;
		}
		
		@Override
		public String toString() {
			return "Employee_bean [empid=" + empid + ", empname=" + empname + ", annualincome=" + annualincome
					+ ", incometax=" + incometax + "]";
		}


		

		

		
	}



