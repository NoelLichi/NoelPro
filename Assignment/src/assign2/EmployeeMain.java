package assign2;
import java.util.*;

public class EmployeeMain {
	
	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
		
		System.out.println("Enter the number of employees to enter:");
		int num=sc.nextInt();
		
		Employee_Vo emp[]= new Employee_Vo[num];
		Employee_Bo bo= new Employee_Bo();
		
		for(int i=0;i<num;i++) {
			emp[i]=new Employee_Vo();
	
		System.out.println("Enter the ID of the Employee:");
		emp[i].setEmpid(sc.nextInt());
		System.out.println("Enter the Name of the employee:");
		emp[i].setEmpname(sc.next());
		System.out.println("Enter the Annual Income of the employee:");
		emp[i].setAnnualincome(sc.nextInt());
		
		bo.calincometax(emp[i]);
	
	}
		
		System.out.println("\n Employees:");
		display(emp);
		System.out.println("\n Employees after sorting income tax in descending order:");
		Collections.sort(Arrays.asList(emp),new Employeesort());
		display(emp);
	}
	
	public static void display(Employee_Vo[] emp) {
			for(Employee_Vo e:emp) {
				System.out.println(e);
			}
	}
	

}
