package com.javatraining.custmodel;

public class BankAccount {
	
	
	private String accountNum;
	private int bal;
	
	@Override
	public String toString() {
		return "BankAccount [accountNum=" + accountNum + ", bal=" + bal + "]";
	}

	public BankAccount() {
		
	}
	
	public BankAccount(String accountNum, int bal) {
		super();
		this.accountNum = accountNum;
		this.bal = bal;
	}
	
	public String getAccountNum() {
		return accountNum;
	}



	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}



	public int getBal() {
		return bal;
	}



	public void setBal(int bal) {
		this.bal = bal;
	}




	

	
	
	

}
