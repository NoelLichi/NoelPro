package com.javatraining.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.javatraining.custmodel.Customer;

public class Client {
	
	public static void main(String[] args) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		
		
		Resource resource=new ClassPathResource("beans.xml");
		BeanFactory factory= new XmlBeanFactory(resource);		
		
//		System.out.println("Enter Client Name:");
//		String a= br.readLine();
		Customer c1=(Customer)factory.getBean("cust1");
//		c.setCustomerName(a);
		Customer c2=(Customer)factory.getBean("cust2");

		System.out.println(c1);

		
		System.out.println(c2);

			
		
		
	}

}
