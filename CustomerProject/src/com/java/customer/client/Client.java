package com.java.customer.client;

import java.util.ArrayList;
import java.util.List;

import com.javatraining.custmodel.Customer;
import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;

public class Client {

	public static void main(String[] args) {
		
//		Customer c= new Customer(999,"Sharukh", "Mumbai", 99000);
		CustomerDAO customerDAO= new CustomerDAOImpl();
		//testing insert method
//		int result=customerDAO.insertCustomer(c);
//		System.out.println(c);
		//testing method update
		
		System.out.println("Testing update method");
		int result=customerDAO.updateCustomer(999, "Soviet",4012);
		System.out.println(result);
		
		
		List <Customer>allCustomers= new ArrayList<Customer>();
		allCustomers = customerDAO.listAllCustomer();
		System.out.println(allCustomers);
		
		
	}

}
