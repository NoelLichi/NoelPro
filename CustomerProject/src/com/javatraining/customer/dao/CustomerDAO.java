package com.javatraining.customer.dao;

import java.util.List;

import com.javatraining.custmodel.Customer;

public interface CustomerDAO {

	
	
	public int insertCustomer(Customer c);//signature to insert customer details
	public int updateCustomer(int customerId, String newcustomerAddress, int newbillAmount); // signature tp update customer details
	public int deleteCustomer(int customerId);//Signature to delete customer details
	public Customer findByCustomerId(int customerId);// signature to find customer find id
	public boolean customerExists(int customerId);// signature to check if the customer exists
	public List<Customer> listAllCustomer();// signature to list all the details
	int updateCustomer(Customer c);
}
