package com.mail;

public class Email {
	
	
	private To to;
	private From from;
	private Subject sub;
	private Body body;
	
	
	public Email() {
		
		
	}


	public Email(To to, From from, Subject sub, Body body) {
		super();
		this.to = to;
		this.from = from;
		this.sub = sub;
		this.body = body;
	}


	public To getTo() {
		return to;
	}


	public void setTo(To to) {
		this.to = to;
	}


	public From getFrom() {
		return from;
	}


	public void setFrom(From from) {
		this.from = from;
	}


	public Subject getSub() {
		return sub;
	}


	public void setSub(Subject sub) {
		this.sub = sub;
	}


	public Body getBody() {
		return body;
	}


	public void setBody(Body body) {
		this.body = body;
	}


	@Override
	public String toString() {
		return "Email [to=" + to + ", from=" + from + ", sub=" + sub + ", body=" + body + "]";
	}
	
	

}
