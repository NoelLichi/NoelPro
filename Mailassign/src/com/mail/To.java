package com.mail;

public class To {
	
	private String ToName, ToEmail;

	public To() {
		
	}

	public To(String toName, String toEmail) {
		super();
		ToName = toName;
		ToEmail = toEmail;
	}

	public String getToName() {
		return ToName;
	}

	public void setToName(String toName) {
		ToName = toName;
	}

	public String getToEmail() {
		return ToEmail;
	}

	public void setToEmail(String toEmail) {
		ToEmail = toEmail;
	}

	@Override
	public String toString() {
		return "To [ToName=" + ToName + ", ToEmail=" + ToEmail + "]";
	}
	
	
	
	
	
	
	
	
}
