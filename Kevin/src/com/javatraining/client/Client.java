package com.javatraining.client;


import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.config.AppConfig;
import com.javatraining.custmodel.BankAccount;
import com.javatraining.custmodel.Customer;

public class Client {
	
	public static void main(String[] args) throws IOException {
//		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		
		
		ApplicationContext context=new AnnotationConfigApplicationContext(AppConfig.class);
			
		Customer c1=context.getBean(Customer.class);
		c1.setCustomerName("Manishi");
		c1.setCustomerId(100);
		c1.setCustomerAddress("Banglore");
		c1.setBillAmnt(5200);
		
		BankAccount b=context.getBean(BankAccount.class);
		b.setAccountNum("abcd1234");
		b.setBal(12000);
		//c1.setBankAccount(b);
		System.out.println(c1);
		

			
		
		
	}

}
