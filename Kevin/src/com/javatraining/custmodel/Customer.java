package com.javatraining.custmodel;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;

public class Customer implements Comparable<Customer> {
	
	private int customerId;
	private String customerName;
	private String customerAddress;
	private int billAmnt;
	
	@Autowired
	private BankAccount bankAccount;
	public Customer() {
		
	}
	
	
	@PostConstruct
	public void a() {
		System.out.println("Are you readyy");
	}
	@PostConstruct
	public void b() {
		System.out.println("Are you mad");
	}
	@PreDestroy
	public void a1() {
		System.out.println("yo yo yo");
	}

	public Customer(int customerId, String customerName, String customerAddress, int billAmnt) {
		super();
		
	
		this.customerId = customerId;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.billAmnt = billAmnt;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public int getBillAmnt() {
		return billAmnt;
	}

	public void setBillAmnt(int billAmnt) {
		this.billAmnt = billAmnt;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", customerName=" + customerName + ", customerAddress="
				+ customerAddress + ", billAmnt=" + billAmnt + ", bankAccount=" + bankAccount + "]";
	}
	
	
	
	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public Customer(int customerId, String customerName, String customerAddress, int billAmnt,
			BankAccount bankAccount) {
		super();
		
		this.customerId = customerId;
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.billAmnt = billAmnt;
		this.bankAccount = bankAccount;
	}

	public Customer(BankAccount a) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + billAmnt;
		result = prime * result + ((customerAddress == null) ? 0 : customerAddress.hashCode());
		result = prime * result + customerId;
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (billAmnt != other.billAmnt)
			return false;
		if (customerAddress == null) {
			if (other.customerAddress != null)
				return false;
		} else if (!customerAddress.equals(other.customerAddress))
			return false;
		if (customerId != other.customerId)
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		return true;
	}

	@Override
	public int compareTo(Customer o) {
		if (this.getCustomerName().compareTo(o.getCustomerName())<0)  //Just change the < and > symbols for ascending and descending.
		return 1;
		else 
		return -1;
	}
	
	
	
	
	
	
	
}
