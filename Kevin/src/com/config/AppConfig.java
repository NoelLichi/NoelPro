package com.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.javatraining.custmodel.BankAccount;
import com.javatraining.custmodel.Customer;


@Configuration
public class AppConfig {
	
	
	@Bean
	@Scope("prototype")

	public Customer getCustomerObject() {
//		BankAccount a=this.getBankaccount();
		return new Customer();
	}
	
	
	@Bean
	public BankAccount getBankaccount() {
		
		return new BankAccount();
	}

}
