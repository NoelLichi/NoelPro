package p1;

import java.text.DecimalFormat;

public class AdditionDF {

	public static void main(String[] args) {
		if(args.length!=2){
			   System.out.println(" Number of args missmatch. Only 2 numbers are accepted");
			return;
		}
		
		DecimalFormat df= new DecimalFormat("#,##,##0.0");
		float a=Float.parseFloat(args[0]);
		float b=Float.parseFloat(args[1]);
		float c= a+b;
		System.out.println("a= "+ df.format(a)+" b= "+ df.format(b)+" sum of a and b= "+ df.format(c));


	}

}
