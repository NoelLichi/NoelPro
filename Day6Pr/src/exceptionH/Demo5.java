package exceptionH;

public class Demo5 {
	public static void main(String[] args) {
		
		String marks="99";
		System.out.println(marks);
		int b= Integer.parseInt(marks);
		System.out.println(b+10);
		
		
		
		//primtive-->object (Boxing)
		int num=10;
		Integer n=new Integer(num);//Boxing
		Integer num1=num;			//AutoBoxing
		
		//object -->primitive
		Integer latscore=100;
		int i=Integer.valueOf(latscore);//UnBoxing
		int scores=latscore;			//AutoUnBoxing
		
		
	}

}
