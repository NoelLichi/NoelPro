package exceptionH;

public class Demo2 {
	String name="Halchal";
	
	public void display() throws InterruptedException {
		System.out.println("Length of the name:"+name.length());
		Thread.sleep(1000);
		System.out.println("Name is :"+name);
	}
	
	public static void main(String[]args) throws InterruptedException {
		Demo2 d=new Demo2();
		d.display();
		Thread.sleep(2000);
		System.out.println("Thank you");
	}

}
