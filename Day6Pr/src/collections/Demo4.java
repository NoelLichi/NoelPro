package collections;

import java.util.*;

public class Demo4 {

	public static void main(String[] args) {
		
		
		Customer c1=new Customer(1,"Mohan","Pune",9800);
		Customer c2=new Customer(2,"Anu","Mumbai",800);
		Customer c3=new Customer(3,"Zeba","Agra",1800);
		Customer c4=new Customer(4,"Uday","Jaipur",2900);
		Customer c5=new Customer(5,"Neeti","Delhi",200);

		List<Customer> allC=new ArrayList<Customer>();
		
		allC.add(c1);
		allC.add(c2);
		allC.add(c3);
		allC.add(c4);
		allC.add(c5);
		
		Collections.sort(allC);
		
		
		
		Iterator i= allC.iterator();
		
		while(i.hasNext()) {
			System.out.println(i.next());
			
			//Noel Lichi's Code
		}
		
	}

}
