package com.javatraining.customer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.javatraining.custmodel.Customer;
import com.javatraining.customer.dao.CustomerDAO;

public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerDAO dao;

	@Override
	public int insertCustomer(Customer c) {
		return dao.insertCustomer(c);
	}

	@Override
	public int updateCustomer(int customerId, String newcustomerAddress, int newbillAmount) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteCustomer(int customerId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Customer findByCustomerId(int customerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean customerExists(int customerId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Customer> listAllCustomer() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updateCustomer(Customer c) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
