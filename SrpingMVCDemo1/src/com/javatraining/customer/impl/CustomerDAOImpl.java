package com.javatraining.customer.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.javatraining.custmodel.Customer;
import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.dbcon.*;


public class CustomerDAOImpl implements CustomerDAO {

	@Override
	public int insertCustomer(Customer c) {
		Connection conn=Dbconfig.getConnection();
	
		int rows=0;
		try {
			PreparedStatement  s = conn.prepareStatement("insert into customer values(?,?,?,?)");
			s.setInt(1, c.getCustomerId());
			s.setString(2, c.getCustomerName());
			s.setString(3, c.getCustomerAddress());
			s.setInt(4, c.getBillAmnt());
			 rows=s.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(rows+ "Data Inserted");
		return rows;
	}

	@Override
	public int updateCustomer(int customerId, String newcustomerAddress, int newbillAmount)
	{
		Connection c= Dbconfig.getConnection();
		
		String q1="update customer set customerAddress=?, billAmount=? where customerId=?";
		int rows=0;
		PreparedStatement s;
		try {
			s = c.prepareStatement(q1);
			s.setInt(3, customerId);
			s.setString(1, newcustomerAddress);
			s.setInt(2, newbillAmount);
			
			 rows= s.executeUpdate();
			System.out.println(rows+ " rows updated");
			
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return rows;
		
	}

	@Override
	public int deleteCustomer(int customerId) {
		int rows=0;
		String deleteQuery="select * from customer where customerId=?";
		Connection c=Dbconfig.getConnection();
		
		try {
			PreparedStatement s=c.prepareStatement(deleteQuery);
			s.setInt(1, customerId);
			rows= s.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	
		return 0;
	}

//	@Override
//	public Customer findByCustomerId(int customerId) {
//		// TODO Auto-generated method stub
//		Connection c=Dbconfig.getConnection();
//		Customer customer=new Customer();
//		ResultSet rs=null;
//		String selectQuery="select * from customer where customerId=?";
//		
//		try {
//			PreparedStatement s=c.prepareStatement(selectQuery);
//			s.setInt(1, customerId);
//			rs=s.executeUpdate();
//			rs.next();
//			customer.setCustomerId(rs.getInt(1));
//			customer.setCustomerName(rs.getString(2));
//			customer.setCustomerAddress(rs.getString(3));
//			customer.setBillAmnt(rs.getInt(4));
//			
//			
//			
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		
//		return customer;
//	}

	@Override
	public boolean customerExists(int customerId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Customer> listAllCustomer() {
		List<Customer>allCustomers=new ArrayList<Customer>();
		Connection c=Dbconfig.getConnection();
		String q1="select * from customer";
		
		try {
			Statement s= c.createStatement();
			ResultSet res=s.executeQuery(q1);
			while (res.next()) {
				
				Customer cus=new Customer();
				cus.setCustomerId(res.getInt(1));
				cus.setCustomerName(res.getString(2));
				cus.setCustomerAddress(res.getString(3));
				cus.setBillAmnt(res.getInt(4));
				allCustomers.add(cus);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return allCustomers;
	}

	@Override
	public int updateCustomer(Customer c) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Customer findByCustomerId(int customerId) {
		// TODO Auto-generated method stub
		return null;
	}


}
