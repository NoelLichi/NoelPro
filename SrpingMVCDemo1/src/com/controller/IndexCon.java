package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.custmodel.Customer;
import com.model.Guest;
@Controller
public class IndexCon {
	
	@RequestMapping("/custForm")
	public ModelAndView getCustomerForm() {
		ModelAndView v=new ModelAndView();
		v.setViewName("CustomerForm");
		v.addObject("command", new Customer());
				return v;
	}
}
