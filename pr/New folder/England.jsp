<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body bgcolor="yellow">

<h1>England History</h1>

The England cricket team represents England and Wales (and, until 1992, also Scotland) in international cricket. Since 1 January 1997 it has been governed by the England and Wales Cricket Board (ECB), having been previously governed by Marylebone Cricket Club (MCC) from 1903 until the end of 1996.[8][9] England, as a founding nation, is a full member of the International Cricket Council (ICC) with Test, One Day International (ODI) and Twenty20 International (T20I) status.

England and Australia were the first teams to play a Test match (between 15–19 March 1877), and these two countries together with South Africa formed the Imperial Cricket Conference (predecessor to today's International Cricket Council) on 15 June 1909. England and Australia also played the first ODI on 5 January 1971. England's first T20I was played on 13 June 2005, once more against Australia.

As of 11 September 2018, England has played 1004 Test matches, winning 361 and losing 298 (with 345 draws). The team has won The Ashes on 32 occasions.[10] England has played 716 ODIs, winning 357,[11] and its record in major ODI tournaments includes finishing as runners-up in three Cricket World Cups (1979, 1987 and 1992), and in two ICC Champions Trophys (2004 and 2013). England has also played 104 T20Is, winning 49.[12] They won the ICC World Twenty20 in 2010, and were runners-up in 2016.

As of 11 September 2018, England are ranked fourth in Tests, first in ODIs and fourth in T20Is by the ICC.[1] Though the team and coaching staff faced heavy criticism after their Group Stage exit in the 2015 Cricket World Cup, it has since adopted a more aggressive and modern playing style in ODI cricket, under the leadership of captain Eoin Morgan and head coach Trevor Bayliss.
</body>
</html>