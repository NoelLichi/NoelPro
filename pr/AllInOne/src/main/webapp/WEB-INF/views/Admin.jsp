<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>

Hello <%out.print(session.getAttribute("name"));%>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Admin.jsp">Welcome...!</a>
    </div>
    <ul class="nav navbar-nav" style="text-align: right;">
      <li class="active"><a>Update Matches</a></li>
      <li style="text-align: left;"><a>Hello <%out.print(session.getAttribute("name"));%></a></li>
    </ul>
  </div>
</nav>

<h1>
	Update a Match.
</h1>

<c:url var="addAction" value="/Admin/add" ></c:url>

<form:form action="${addAction}" commandName="matchinfo">
<table>
	<tr>
		<td>
			Match ID :
		</td>
		<td>
			<form:input path="matchid" />
			
		</td> 
	</tr>
	<tr>
		<td>
			Team1
		</td>
		<td>
		<!--  <form:select path="team1id" items="${listteams}" >
			
			</form:select>-->	
			
			
			<form:select path="team1id" multiple="true" id="componentId">
	<c:forEach items="${teamA}" var="team">
    <form:option value="${team.teamid} " label="${team.teamname} " />
    	</c:forEach>
</form:select>
		</td> 
	</tr>
	<tr>
		<td>
			Team2
		</td>
		<td>
			<form:select path="team2id" multiple="true" id="componentId">
	<c:forEach items="${teamA}" var="team">
    <form:option value="${team.teamid} " label="${team.teamname} " />
    	</c:forEach>
</form:select>
		</td> 
	</tr>
	<tr>
		<td>
			Match Date
		</td>
		<td>
			<form:input path="matchdate" type="date"/>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${!empty matchinfo.matchid}">
				<input type="submit" 
				formaction="add/update"
					value="Edit Match"/>
			</c:if>
			<c:if test="${empty matchinfo.matchid}">
				<input type="submit"
					value="Add Match"/>
			</c:if>
		</td>
	</tr>
</table>
</form:form>
<br>
<h3>Persons List</h3>
<c:if test="${!empty listmatch}">
	<table class="tg">
	<tr>
		<th width="80">Match ID</th>
		<th width="120">Team 1</th>
		<th width="120">Team 2</th>
		<th width="120">Match Date</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listmatch}" var="match">
		<tr>
			<td>${match.matchid}</td>
			<td>${match.team1id}</td>
			<td>${match.team2id}</td>
			<td>${match.matchdate}</td>
			<td><a href="<c:url value='/edit/${match.matchid}' />" >Edit</a></td>
			<td><a href="<c:url value='/remove/${match.matchid}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
${matchinfo}
</body>
</html>