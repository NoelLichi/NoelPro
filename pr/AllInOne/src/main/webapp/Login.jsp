<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Insert title here</title>
<style>
body  {
     background-image: url("resources/images/crick.jpg");
     background-position: center; /* Center the image */
  background-repeat: no-repeat; /* Do not repeat the image */
  background-size: 100%;
  font-weight: 900;
   
    
   
}
</style>

</head>
<body>
<form:form method="post" action="LoginServlet">        
       
        

<div class="container" style="font-size: 130%;color: #333">
  <h2><center>LOG IN</center></h2>
  <p><center>Please Enter Your Details</center></p>            
  <table class="table">
    
    <tbody>
      <tr>
        <td>Username</td>
        <td><input type="text" name="user" /><br/><br/></td>
        
      </tr>
      <tr>
        <td>Password</td>
        <td><input type="password" name="pass" /></td>
        
      </tr>
      <tr>
     
        <td> <center><input type="submit" value="Login" /></center></td>
        <td> <center><a href="signup.do">New User?SignUp</a></center></td>
        
      </tr>
    </tbody>
  </table>
</div>
        
        
        
</form:form>
</body>
</html>