<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>
South Africa History 
</h1><br>

The South African national cricket team, nicknamed the Proteas (after South Africa's national flower, Protea cynaroides, commonly known as the "king protea"), is administered by Cricket South Africa. South Africa is a full member of the International Cricket Council (ICC) with Test, One Day International (ODI) and Twenty20 International (T20I) status.

South Africa entered first-class and international cricket at the same time when they hosted an England cricket team in the 1888–89 season. At first, the team was no match for Australia or England but, having gained in experience and expertise, they were able to field a competitive team in the first decade of the 20th century. The team regularly played against Australia, England and New Zealand through to the 1960s, by which time there was considerable opposition to the country's apartheid policy and an international ban was imposed by the ICC, commensurate with actions taken by other global sporting bodies. When the ban was imposed, South Africa had developed to a point where its team including Eddie Barlow, Graeme Pollock and Mike Procter was arguably the best in the world and had just outplayed Australia.

The ban remained in place until 1991 and South Africa could then play against India, Pakistan, Sri Lanka and the West Indies for the first time. The team since reinstatement has mostly been strong and has at times held number one positions in international rankings but has lacked success in organized tournaments. Outstanding players since reinstatement have included Allan Donald, Makhaya Ntini, Shaun Pollock, Jacques Kallis, Graeme Smith, Kagiso Rabada, AB de Villiers, Dale Steyn and Hashim Amla.
</body>
</html>