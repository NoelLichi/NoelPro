<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>CricketApp | RANKINGS</title>

<style>
body
{
 font-size: xx-large;
 
}
</style>
</head>

<body bgcolor="black">
<img src="C:\Users\nexwave\Desktop\c1.jpg" width="1000">

<font color="white">
	<!-- header begins-->


	<!-- header ends-->

	<section id="pages">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="pages">
					<div class="container" role="main">
						<h1 class="text-center typo"><i><u>ICC Team Rankings For Test, ODI,
							T20</i></u></h1>
						<h3 class="text-center">ICC Test Championship</h3>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped" cellspacing="20px">
									<thead>
										<tr align="center">
											<th>Position</th>
											<th>Team</th>
											<th>Ratings</th>
											<th>Points</th>
										</tr>
									</thead>
									<tbody>
										<tr align="center">
											<td>1</td>
											<td>India</td>
											<td>125</td>
											<td>3634</td>
										</tr>
										<tr align="center">
											<td>2</td>
											<td>South Africa</td>
											<td>106</td>
											<td>3712</td>
										</tr>
										<tr align="center">
											<td>3</td>
											<td>Australia</td>
											<td>106</td>
											<td>3499</td>
										</tr>
										<tr align="center">
											<td>4</td>
											<td>New Zealand</td>
											<td>102</td>
											<td>2354</td>
										</tr>
										<tr align="center">
											<td>5</td>
											<td>England</td>
											<td>97</td>
											<td>3772</td>
										</tr>
										<tr align="center">
											<td>6</td>
											<td>Sri Lanka</td>
											<td>97</td>
											<td>3668</td>
										</tr>
										<tr align="center">
											<td>7</td>
											<td>Pakistan</td>
											<td>88</td>
											<td>1853</td>
										</tr>
										<tr align="center">
											<td>8</td>
											<td>West Indies</td>
											<td>77</td>
											<td>2235</td>
										</tr>
										<tr align="center">
											<td>9</td>
											<td>Bangladesh</td>
											<td>67</td>
											<td>1268</td>
										</tr>
										<tr align="center">
											<td>10</td>
											<td>Zimbawe</td>
											<td>2</td>
											<td>12</td>
										</tr>

									</tbody>
								</table>
							</div>
							<h3 class="text-center">ICC ODI Championship</h3>
							<div class="col-md-12">
								<table class="table table-striped" cellspacing="20px">
									<thead>
										<tr>
											<th>Position</th>
											<th>Team</th>
											<th>Ratings</th>
											<th>Points</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>England</td>
											<td>127</td>
											<td>6470</td>
										</tr>
										<tr>
											<td>2</td>
											<td>India</td>
											<td>121</td>
											<td>5819</td>
										</tr>
										<tr>
											<td>3</td>
											<td>South Africa</td>
											<td>114</td>
											<td>4221</td>
										</tr>
										<tr>
											<td>4</td>
											<td>New Zealand</td>
											<td>112</td>
											<td>4602</td>
										</tr>
										<tr>
											<td>5</td>
											<td>Paklistan</td>
											<td>104</td>
											<td>3844</td>
										</tr>
										<tr>
											<td>6</td>
											<td>Australia</td>
											<td>100</td>
											<td>3699</td>
										</tr>
										<tr>
											<td>7</td>
											<td>Bangladesh</td>
											<td>92</td>
											<td>2477</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Sri Lanka</td>
											<td>76</td>
											<td>3492</td>
										</tr>
										<tr>
											<td>9</td>
											<td>West Indies</td>
											<td>69</td>
											<td>2217</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Afganistan</td>
											<td>63</td>
											<td>1758</td>
										</tr>
							


									</tbody>
								</table>
							</div>
						</div>

						<h3 class="text-center">ICC T20 Championship</h3>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped" cellspacing="20px">
									<thead>
										<tr>
											<th>Position</th>
											<th>Matches</th>
											<th>Points</th>
											<th>Ratings</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Pakistan</td>
											<td>132</td>
											<td>3972</td>
										</tr>
										<tr>
											<td>2</td>
											<td>India</td>
											<td>124</td>
											<td>4601</td>
										</tr>
										<tr>
											<td>3</td>
											<td>Australia</td>
											<td>122</td>
											<td>2570</td>
										</tr>
										<tr>
											<td>4</td>
											<td>England</td>
											<td>117</td>
											<td>2448</td>
										</tr>
										<tr>
											<td>5</td>
											<td>New Zealand</td>
											<td>116</td>
											<td>2542</td>
										</tr>
										<tr>
											<td>6</td>
											<td>South Africa</td>
											<td>114</td>
											<td>2058</td>
										</tr>
										<tr>
											<td>7</td>
											<td>West Indies</td>
											<td>106</td>
											<td>2219</td>
										</tr>
										<tr>
											<td>8</td>
											<td>Afghanistan</td>
											<td>91</td>
											<td>2287</td>
										</tr>
										<tr>
											<td>9</td>
											<td>Sri Lanka</td>
											<td>85</td>
											<td>2287</td>
										</tr>
										<tr>
											<td>10</td>
											<td>Bangladesh</td>
											<td>77</td>
											<td>2066</td>
										</tr>
									
									</tbody>
								</table>
							</div>


						
	</script>
</font>
</body>

</html>