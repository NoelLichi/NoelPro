package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="match")
public class Match {
	
	@Id
	@Column(name="matchid")
	private String matchid;
	
	@Column
	private String team1id;
	
	@Column
	private String team2id;
	
	@Column
	private String matchdate;

	
	public Match() {
		super();
	}


	public String getMatchid() {
		return matchid;
	}


	public void setMatchid(String matchid) {
		this.matchid = matchid;
	}


	public String getTeam1id() {
		return team1id;
	}


	public void setTeam1id(String team1id) {
		this.team1id = team1id;
	}


	public String getTeam2id() {
		return team2id;
	}


	public void setTeam2id(String team2id) {
		this.team2id = team2id;
	}


	public String getMatchdate() {
		return matchdate;
	}


	public void setMatchdate(String matchdate) {
		this.matchdate = matchdate;
	}


	public Match(String matchid, String team1id, String team2id, String matchdate) {
		super();
		this.matchid = matchid;
		this.team1id = team1id;
		this.team2id = team2id;
		this.matchdate = matchdate;
	}


	@Override
	public String toString() {
		return "Match [matchid=" + matchid + ", team1id=" + team1id + ", team2id=" + team2id + ", matchdate="
				+ matchdate + "]";
	}



	
	
	
	


}
