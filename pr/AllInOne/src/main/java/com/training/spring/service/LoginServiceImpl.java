package com.training.spring.service;

import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.LoginDAO;
import com.training.spring.model.Login;

public class LoginServiceImpl implements LoginService {

	
	private LoginDAO loginDAO;

	public void setLoginDAO(LoginDAO loginDAO) {
		this.loginDAO = loginDAO;
	}
	
	@Override
	@Transactional
	public void insert(Login l) {

		this.loginDAO.insert(l);
	}

}
