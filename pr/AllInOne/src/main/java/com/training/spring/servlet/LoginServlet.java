package com.training.spring.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String user = request.getParameter("user");
        String pass = request.getParameter("pass");
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@127.0.0.1:1521:orcl","scott","tiger");
            PreparedStatement pst = conn.prepareStatement("Select * from Login where username=? and password=?");
            pst.setString(1, user);
            pst.setString(2, pass);
            ResultSet rs = pst.executeQuery();
       
            if (rs.next()) {
              //  out.println("Correct login credentials");
                if(user.equals("admin") && pass.equals("admin")) 
              {
                	System.out.println("#####################CORRECT");
                HttpSession session=request.getSession();
                session.setAttribute("name", user);
                
                RequestDispatcher dispatcher=request.getRequestDispatcher("/Admin.do");
                dispatcher.include(request, response);
            } 
            else
            {
            	HttpSession session=request.getSession();
            	session.setAttribute("name", user);
            	
                RequestDispatcher dispatcher=request.getRequestDispatcher("User.jsp");
                dispatcher.include(request, response);
            }
            }
            
            else {
             //   out.println("Incorrect login credentials");
            }
        } 
        catch (SQLException e) {
            e.printStackTrace();
        } 
        
        catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
