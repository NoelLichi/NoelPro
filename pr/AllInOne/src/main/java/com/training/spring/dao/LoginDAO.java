package com.training.spring.dao;

import com.training.spring.model.Login;

public interface LoginDAO {

	public void insert(Login l);
}
