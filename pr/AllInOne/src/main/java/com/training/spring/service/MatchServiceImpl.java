package com.training.spring.service;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.MatchDAO;
import com.training.spring.model.Match;
@Service
public class MatchServiceImpl implements MatchService {


	public MatchDAO getMatchDAO() {
		return matchDAO;
	}

	public void setMatchDAO(MatchDAO matchDAO) {
		this.matchDAO = matchDAO;
	}

	private MatchDAO matchDAO;

	public void setLoginDAO(MatchDAO matchDAO) {
		this.matchDAO = matchDAO;
	}
	
	@Override
	@Transactional
	public void insert(Match m) {

		this.matchDAO.insert(m);
	}

	@Override
	@Transactional
	public List<Match> listmatchs() {
		return this.matchDAO.listmatchs();
	}

}
