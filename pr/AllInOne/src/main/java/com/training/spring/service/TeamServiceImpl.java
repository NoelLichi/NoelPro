package com.training.spring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.training.spring.dao.TeamDAO;
import com.training.spring.model.Team;
@Service
public class TeamServiceImpl implements TeamService {
	

	private TeamDAO teamDAO;

	public TeamDAO getTeamDAO() {
		return teamDAO;
	}

	public void setTeamDAO(TeamDAO teamDAO) {
		this.teamDAO = teamDAO;
	}


	@Override
	@Transactional
	public List<Team> listteams() {
		return this.teamDAO.listteams();
	}

}
