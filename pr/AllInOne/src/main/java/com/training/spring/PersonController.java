package com.training.spring;

import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.training.spring.model.Login;
import com.training.spring.model.Match;
import com.training.spring.model.Person;
import com.training.spring.model.Team;
import com.training.spring.service.LoginService;
import com.training.spring.service.MatchService;
import com.training.spring.service.PersonService;
import com.training.spring.service.TeamService;

@Controller
public class PersonController {
	
	private PersonService personService;
	
	@Autowired
	private LoginService loginService;
	
	@Autowired
	private MatchService matchService;
	
	@Autowired
	private TeamService teamService;
	
	
	@Autowired(required=true)
	@Qualifier(value="personService")
	public void setPersonService(PersonService ps){
		this.personService = ps;
	}
	@RequestMapping(value = {"/login","/"}, method = RequestMethod.GET)
	public String listPersons1(Model model) {
		//model.addAttribute("person", new Person());
		//model.addAttribute("listPersons", this.personService.listPersons());
		return "Login";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String listPersons2(Model model) {
		model.addAttribute("command", new Login());
	//	model.addAttribute("listPersons", this.personService.listPersons());
		return "signup";
	}
	
	
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	public ModelAndView loginService(@ModelAttribute("login") Login l) {
		//model.addAttribute("person", new Person());
		//model.addAttribute("person", new Person());
		//Session session=new AnnotationConfiguration().configure().buildSessionFactory().openSession();
		//session.add
		ModelAndView view = new ModelAndView();
		view.setViewName("Login");
		view.addObject("command", new Login());
		this.loginService.insert(l);
		return view;
	}
	
//	@RequestMapping(value = "/signin", method = RequestMethod.GET)
//	public String signin(Model model) {
//		model.addAttribute("command", new Login());
//		model.addAttribute("signin", this.signin(model));
//		return "Login";
//	}
//	
	
	@RequestMapping(value = {"/persons","/Admin"}, method = RequestMethod.POST)
	public String Adminmatch(Model model) {
		//model.addAttribute("person", new Person());
		//model.addAttribute("listPersons", this.personService.listPersons());
		System.out.println("ok");
		model.addAttribute("matchinfo",new Match());
		model.addAttribute("teamA",teamService.listteams());
		//model.addAttribute("listmatch",this.matchService.listmatchs());
		
		return "Admin";
	}
	
	
//	@RequestMapping(value = {"/persons","/Admin"}, method = RequestMethod.GET)
//	public String listPersons(Model model) {
//		//model.addAttribute("person", new Person());
//		//model.addAttribute("listPersons", this.personService.listPersons());
//		model.addAttribute("matchinfo",new Match());
//		model.addAttribute("listmatch",this.matchService.listmatchs());
//		
//		return "person";
//	}
	
	
	
//	@RequestMapping("/Admin")
//	public String listteams(Model model) {
//		System.out.println("#######I M LIST");
//		//model.addAttribute("person", new Person());
//		//model.addAttribute("listPersons", this.personService.listPersons());
//		model.addAttribute("matchinfo",new Team());
//		model.addAttribute("listteams",this.teamService.listteams());
//		
//		return "listteams";
//	}
	
	
	
	
	
	//For add and update person both
	@RequestMapping(value= "/Admin/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("match") Match m){
		System.out.println("#####match :"+m);
		//	this.personService.addPerson(p);
		this.matchService.insert(m);
		return "redirect:/Admin";
	}

	@RequestMapping(value= "/edit/add/update", method = RequestMethod.POST)
	public String updatePerson(@ModelAttribute("person") Person p){
		System.out.println("#####product updating :"+p);
			this.personService.updatePerson(p);
		return "redirect:/persons";
	}
	
	
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") int id){
		
        this.personService.removePerson(id);
        return "redirect:/persons";
    }
 
    @RequestMapping("/edit/{id}")
    public String editPerson(@PathVariable("id") int id, Model model){
        model.addAttribute("person", this.personService.getPersonById(id));
        model.addAttribute("listPersons", this.personService.listPersons());
        return "person";
    }
	
}
