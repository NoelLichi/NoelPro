package com.training.spring.service;

import java.util.List;

import com.training.spring.model.Team;

public interface TeamService {
	
	public List<Team> listteams();

}
