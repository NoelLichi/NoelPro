package com.training.spring.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.training.spring.model.Login;

public class LoginDAOImpl implements LoginDAO {

	private static final Logger logger = LoggerFactory.getLogger(LoginDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void insert(Login l) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(l);
		logger.info("User saved successfully, Person Details="+l);
	}

}
