package com.training.spring.dao;

import java.util.List;

import com.training.spring.model.Team;

public interface TeamDAO {
	
	public List<Team> listteams();

}
