package com.training.spring.service;

import java.util.List;

import com.training.spring.model.Match;

public interface MatchService {
	
	public void insert(Match m);
	public List<Match> listmatchs();

}
