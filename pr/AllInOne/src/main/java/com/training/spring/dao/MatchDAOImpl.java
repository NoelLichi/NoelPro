package com.training.spring.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.training.spring.model.Match;
import com.training.spring.model.Person;
import com.training.spring.model.Team;

public class MatchDAOImpl implements MatchDAO {

	private static final Logger logger = LoggerFactory.getLogger(LoginDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void insert(Match m) {
		
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(m);
		logger.info("User saved successfully, Person Details="+m);

	}
	
	

	@Override
	public List<Match> listmatchs() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Match> matchlist = session.createQuery("from match").list();
		for(Match m : matchlist){
			logger.info("Match List::"+m);
		}
		return matchlist;
	}

	

}
