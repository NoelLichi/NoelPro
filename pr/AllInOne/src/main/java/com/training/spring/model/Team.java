package com.training.spring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="team")
public class Team {
	
	@Id
	@Column(name="teamid")
	private String teamid;
	
	@Column
	private String teamname;

	
	public Team() {
		super();
	}


	public Team(String teamid, String teamname) {
		super();
		this.teamid = teamid;
		this.teamname = teamname;
	}


	public String getTeamid() {
		return teamid;
	}


	public void setTeamid(String teamid) {
		this.teamid = teamid;
	}


	public String getTeamname() {
		return teamname;
	}


	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}


	@Override
	public String toString() {
		return "Team [teamid=" + teamid + ", teamname=" + teamname + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((teamid == null) ? 0 : teamid.hashCode());
		result = prime * result + ((teamname == null) ? 0 : teamname.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (teamid == null) {
			if (other.teamid != null)
				return false;
		} else if (!teamid.equals(other.teamid))
			return false;
		if (teamname == null) {
			if (other.teamname != null)
				return false;
		} else if (!teamname.equals(other.teamname))
			return false;
		return true;
	}
	
	

}
