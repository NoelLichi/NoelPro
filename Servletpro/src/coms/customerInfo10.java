package coms;

import java.io.IOException;
import com.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.javatraining.custmodel.Customer;
import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;

/**
 * Servlet implementation class customerInfo10
 */
public class customerInfo10 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public customerInfo10() {
        super();
        // TODO Auto-generated constructor stub
    }
    

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int id= Integer.parseInt(request.getParameter("customerId"));
		String name=request.getParameter("customerName");
		String add=request.getParameter("customerAddress");
		int bill=Integer.parseInt(request.getParameter("billAmount"));
		
		
		Customer c=new Customer(id, name, add, bill);
		CustomerDAO dao=new CustomerDAOImpl();
		dao.insertCustomer(c);
	
		response.getWriter().println(id+"<br>");
		response.getWriter().println(name+"<br>");
		response.getWriter().println(add+"<br>");
		response.getWriter().println(bill+"<br>");
	
	}

}
