package coms;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Servlet implementation class Welcome
 */
public class Wel extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Wel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
    
    int counter=0;
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		counter++;
		String username=request.getParameter("uname");
		String pass=request.getParameter("pass");
	//(username.equalsIgnoreCase("ram"))
		
		boolean alreadyVisited=false;
		Cookie[] allcookies=request.getCookies();
		
		if(allcookies!=null)
		{
			for(Cookie c:allcookies)
			{
				if(c.getName().equals(username) && c.getValue().equals(pass))
					{
						alreadyVisited=true;
					}
			}
		}
		if(alreadyVisited)
		{
			
			response.getWriter().println("Welcome ,"+username);
			response.getWriter().println("You have already visited this page");
//			HttpSession s=request.getSession();
//			s.setAttribute("uname", username);
			
//			RequestDispatcher rd= request.getRequestDispatcher("Enter");
//			rd.forward(request, response);
		
//		response.getWriter().println("You are my "+counter+ " visitor");
		
		}
		else
		{
			response.getWriter().println("Welcome ,"+username);
			
			response.getWriter().println("You have visited for the first time");
			Cookie cookie=new Cookie(username, pass);
			response.addCookie(cookie);
		}
		
		
	}

}
