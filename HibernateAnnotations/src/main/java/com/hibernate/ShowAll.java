package com.hibernate;



import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.javatraining.custmodel.Customer;

/**
 * Hello world!
 *
 */
public class ShowAll 
{
    public static void main( String[] args )
    {
    	Customer c= new Customer();
//    	Customer c=new Customer(5, "Homeless", "NoHome", 00000);
//    	c.setCustomerId(5);
//		This will search for the hibernate.cfg.xml and load the dbconfiguartion
    	AnnotationConfiguration config=new AnnotationConfiguration();
    	SessionFactory sf=config.configure().0buildSessionFactory();
    	Session s=sf.openSession();
    	System.out.println("All Customer Details");
    	
    	Query q=s.createQuery("from Customer");
//    	Criteria q=s.createCriteria(Customer.class)
//    							.add(Restrictions.gt("billAmnt", 1000));
    	
    	
    	
    	List<Customer> customers=q.list();
    	
    	Iterator<Customer> i=customers.iterator();
    	while (i.hasNext()) 
    	{
    		Customer cust=i.next();
    		System.out.println(cust);
		}
    	s.close();
    	sf.close();
    	
    }
}
