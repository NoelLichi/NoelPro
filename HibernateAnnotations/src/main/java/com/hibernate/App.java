package com.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import com.javatraining.custmodel.Customer;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
//    	Customer c= new Customer();
    	Customer c=new Customer(2, "Lal", "Assam", 3000);
//    	c.setCustomerId(5);
//		This will search for the hibernate.cfg.xml and load the dbconfiguartion
    	AnnotationConfiguration config=new AnnotationConfiguration();
    	
    	SessionFactory sf=config.configure().buildSessionFactory();
    	Session s=sf.openSession();
    	Transaction tr=s.beginTransaction();
    	s.save(c);
    	tr.commit();
    	System.out.println("Data stored");
    	s.close();
    	sf.close();
    	
    }
}
