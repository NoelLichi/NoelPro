//$Id: Simple.java 11282 2007-03-14 22:05:59Z epbernard $
package org.hibernate.ejb.test.instrument;

/**
 * @author Emmanuel Bernard
 */
public class Simple {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
